This project will accept (merge/rebase/squash) *all*
contributions. Contributions that break CI (once it is
introduced) will be reverted.

For details, please see [Why Optimistic Merging Works
Better](http://hintjens.com/blog:106).


